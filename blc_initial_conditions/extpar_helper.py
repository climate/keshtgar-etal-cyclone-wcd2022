import xarray as xr
import numpy as np

ds = xr.open_dataset('extpar_remapped.nc')

time_array = np.array([1.111011e+07, 1.111021e+07, 1.111031e+07, 1.111041e+07, 1.111051e+07,1.111061e+07, 1.111071e+07, 1.111081e+07,1.111091e+07, 1.111101e+07,1.111111e+07, 1.111121e+07])

newds_list = []
for ind in range(0,12):
    temp = ds.copy(deep=True)
    temp['time']= temp['time']*0 + time_array[ind]
    newds_list.append(temp)

temp = ds.copy(deep=True)

newds = xr.merge(newds_list)

newds['SOILTYP'] = newds['SOILTYP'].astype('int32', copy=True)

newds.attrs = ds.attrs

for var in ds.data_vars.keys():
    newds[var].attrs = ds[var].attrs

newds['T_CL'][:] = 285

newds.to_netcdf('extpar_remapped_12_months.nc')
