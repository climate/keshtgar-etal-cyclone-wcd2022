#!/bin/bash
#=============================================================================
#SBATCH --account=bb1135
#SBATCH --job-name=remap_extpar
#SBATCH --partition=compute
#SBATCH --nodes=2
#SBATCH --threads-per-core=2
#SBATCH --mem=120000
#SBATCH --output=/work/bb1135/b381185/simulation_setup/LC1_Limited_channel/LOG.initial_conditions2.run.%j.o
#SBATCH --error=/work/bb1135/b381185/simulation_setup/LC1_Limited_channel/LOG.initial_conditions2.run.%j.o
#SBATCH --exclusive
#SBATCH --time=01:00:00
#=========================================================================================
# This script is for preparing the initial files needed for baroclinic life cycle 
# simulation using ICONTOOLS.
#=========================================================================================
#-----------------------------------------------------------------------------------------
# Introduce switches to decide what the script will do.
# Switches can be set to "yes" or "no".

# remap extpar data
remap_extpar=yes

# remap ozone data
remap_ozone=yes

#-----------------------------------------------------------------------------------------
# Load modules 

module purge
module load anaconda3/bleeding_edge
module load nco/4.7.5-gcc64
module load ncl/6.5.0-gccsys

#--------------------------------------------------------------------------------------
# ICONTOOLS directory

ICONTOOLS_DIR=/pf/b/b381185/ICON/icon_tutorial/dwd_icon_tools/icontools

BINARY_ICONSUB=iconsub_mpi
BINARY_REMAP=iconremap_mpi
BINARY_GRIDGEN=icongridgen

#--------------------------------------------------------------------------------------
# file with channel grid

gridfile=Channel_4000x9000_80000m_with_boundary.nc

cd planar_channel_51x81_80km
#--------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------
# 1- Remap the aquaplanet extpar file onto the channel grid

# NOTE: do not use indent for "EOF"; otherwise the calculation terminates with an error
if [[ "$remap_extpar" == "yes" ]]; then
    echo "#####################################################"
    echo "Remap extpar file onto channel grid"
    echo "#####################################################"

    for field in SOILTYP FR_LAND ICE PLCOV_MX LAI_MX RSMIN URBAN FOR_D FOR_E EMIS_RAD ROOTDP Z0 NDVI_MAX topography_c SSO_STDH SSO_THETA SSO_GAMMA SSO_SIGMA T_CL FR_LAKE DEPTH_LK topography_v LU_CLASS_FRACTION NDVI NDVI_MRAT AER_BC AER_DUST AER_ORG AER_SO4 AER_SS ALB ALNID ALUVD lon lat clon clat clon_vertices clat_vertices ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF
    
    done
    
    #cat NAMELIST_ICONREMAP_FIELDS
    
    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = '../inputs/icon_grid_0010_R02B04_G.nc'
     in_filename       = '../inputs/icon_extpar_0010_R02B04_G_aquaplanet.nc'
     in_type           = 2
     out_grid_filename = '../inputs/${gridfile}'
     out_filename      = 'extpar_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF
    
    # run DWD ICONTOOLS
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # Correction of remapped extpar file, so that ICON can understand it
    ncatted -a rawdata,global,c,c,"GLOBCOVER2009, FAO DSMW, GLOBE, Lake Database" extpar_remapped.nc
    
    python ../extpar_helper.py

    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
    rm extpar_remapped.nc
fi

#-----------------------------------------------------------------------------
#-----------------------------------------------------------------------------
# 2- Remap the Ozone file onto the channel grid

if [[ "$remap_ozone" == "yes" ]]; then
    echo ""
    echo "#####################################################"
    echo "Remap ozone file onto channel grid"
    echo "#####################################################"
    
    for field in O3 ; do
    
    cat >> NAMELIST_ICONREMAP_FIELDS << EOF
    !
    &input_field_nml
     inputname      = "${field}"
     outputname     = "${field}"
     intp_method    = 3
    /
EOF
    
    done
    
    cat > NAMELIST_ICONREMAP << EOF
    &remap_nml
     in_grid_filename  = ''
     in_filename       = '../inputs/ape_o3_R2B04_1Pa_cell.t63grid.nc'
     in_type           = 1
     out_grid_filename = '../inputs/${gridfile}'
     out_filename      = 'ape_O3_remapped.nc'
     out_type          = 2
     out_filetype      = 4
     l_have3dbuffer    = .false.
     ncstorage_file    = "ncstorage.tmp"
    /
EOF

    # run DWD ICONTOOLS    
    ${ICONTOOLS_DIR}/${BINARY_REMAP} \
                --remap_nml NAMELIST_ICONREMAP                                  \
                --input_field_nml NAMELIST_ICONREMAP_FIELDS 2>&1
    
    # Correction of remapped ozone file
    ncrename -d plev,level ape_O3_remapped.nc
    ncrename -v plev,level ape_O3_remapped.nc

    # clean-up
    rm -f ncstorage.tmp*
    rm -f nml.log  NAMELIST_SUB NAMELIST_ICONREMAP NAMELIST_ICONREMAP_FIELDS
fi

#
