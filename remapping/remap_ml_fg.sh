#!/bin/bash
#=============================================================================
#SBATCH --account=bb1135
#SBATCH --job-name=remap_nn.run
#SBATCH --partition=compute
#SBATCH --nodes=10
#SBATCH --threads-per-core=2
#SBATCH --mem=120000
#SBATCH --time=08:00:00                 # The time the job will take to run.
#SBATCH --mail-type=END                 # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=behrooz.keshtgar@kit.edu
#SBATCH --output=/scratch/b/b381185/output/LC1-channel-4000x9000km-2km-0002_remapped/LOG.remap_nn.run.%j.o
#SBATCH --error=/scratch/b/b381185/output/LC1-channel-4000x9000km-2km-0002_remapped/LOG.remap_nn.run.%j.o
#SBATCH --exclusive
#=========================================================================================================================

# this script is relevant for simulations 0002 and 0003 since I output First guess data instead of a selection of variables (atm3d)

# this script is for remapping data sets from the native unstructured grid
# to regular lat/lon grid
# first near neighbor interpolation with the same resolution then conservative remapping

# 1) Creating weights from a dataset that contains grid information!
# icon-extra_ml.nc is the dataset that contains the clon,clat from the grid file itself

# Grid description: grid_desc_2.5km

cdo -P 32 gennn,grid_desc_2.5km icon-extra_ml.nc weights_nn_2.5km.nc

# we use this weight file to remap other simulations

#-----------------------------------------------------------------------------------------
mkdir LC1-channel-4000x9000km-2km-0003_remapped
cd LC1-channel-4000x9000km-2km-0003_remapped

# 2) selecting, varaibles from the fg datasets 
# loop over the time steps

for day in 01 02 03 04 05 06 07 08 09 10 ; do

for hour in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ; do


# sselecting vars to interpolate
cdo -P 38 selname,u,v,pres,w,omega,pv,temp,vor,rho ./icon-fg_ML_202101${day}T${hour}0000Z.nc ./icon-3d_ML_202101${day}T${hour}0000Z.nc

# if 'vn' is present in the dataset remove it
#cdo -P 8 -delvar,vn ./icon-fg_ML_202101${day}T${hour}0000Z.nc ./icon-3d_ML_202101${day}T${hour}0000Z.nc

# 3) remap to regular lat,lon grid (near neighbor)

cdo -P 38 remap,../grid_desc_2.5km,../weights_nn_2.5km.nc ./icon-3d_ML_202101${day}T${hour}0000Z.nc ./icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

rm ./icon-3d_ML_202101${day}T${hour}0000Z.nc

# 4) conservative remapping to 0.5x0.5 degree

cdo -P 32 remapcon,../grid_desc_0.5x0.5 ./icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc ./icon-atm3d_ML_reg_con_0.5x0.5_202101${day}T${hour}0000Z.nc

# 5) conservative remapping to 1x1 degree

cdo -P 32 remapcon,../grid_desc_1x1 ./icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc ./icon-atm3d_ML_reg_con_1x1_202101${day}T${hour}0000Z.nc

rm ./icon-atm3d_ML_reg_202101${day}T${hour}0000Z.nc

done

done
