# interpolating from model levels to pressure levels

mkdir datasets_pres
cd datasets_pres

# looping through day and hours

for day in 01 02 03 04 05 06 07 08 09 10; do

for hour in 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ; do

# first getting surface pressure/pres from atm3d/fg dataset
# if 'pres and pres_sfc' is in the date sets then you can skip this part

cdo -P 38 selname,pres_sfc,pres ../icon-fg_ML_reg_con_202101${day}T${hour}0000Z.nc ./icon-pres_202101${day}T${hour}0000Z.nc

# merge datasets
cdo -P 38 merge ../icon-ddt_wind_ML_reg_con_202101${day}T${hour}0000Z.nc ./icon-pres_202101${day}T${hour}0000Z.nc ./icon-ddtwp_202101${day}T${hour}0000Z.nc

cdo -P 38 merge ../icon-ddt_temp_ML_reg_con_202101${day}T${hour}0000Z.nc ./icon-pres_202101${day}T${hour}0000Z.nc ./icon-ddttp_202101${day}T${hour}0000Z.nc

cdo -P 38 merge ../icon-atm3d_ML_reg_con_202101${day}T${hour}0000Z.nc ./icon-pres_202101${day}T${hour}0000Z.nc ./icon-atm_202101${day}T${hour}0000Z.nc

# interpolation
cdo -P 38 ap2plx,5000,7000,10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000,65000,70000,75000,80000,85000,90000,92500,95000,97500,100000 ./icon-ddtwp_202101${day}T${hour}0000Z.nc ./icon-ddt_wind_PL_reg_con_202101${day}T${hour}0000Z.nc

cdo -P 38 ap2plx,5000,7000,10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000,65000,70000,75000,80000,85000,90000,92500,95000,97500,100000 ./icon-ddttp_202101${day}T${hour}0000Z.nc ./icon-ddt_temp_PL_reg_con_202101${day}T${hour}0000Z.nc

cdo -P 38 ap2plx,5000,7000,10000,15000,20000,25000,30000,35000,40000,45000,50000,55000,60000,65000,70000,75000,80000,85000,90000,92500,95000,97500,100000 ./icon-atm_202101${day}T${hour}0000Z.nc ./icon-atm3d_PL_reg_con_202101${day}T${hour}0000Z.nc


# removing unnecessary datasets
rm ./icon-pres_202101${day}T${hour}0000Z.nc

rm ./icon-ddtwp_202101${day}T${hour}0000Z.nc

rm ./icon-ddttp_202101${day}T${hour}0000Z.nc

rm ./icon-atm_202101${day}T${hour}0000Z.nc

done

done



