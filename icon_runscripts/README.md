**ICON simulations**

LC1-channel-4000x9000km-2km-0002 : no radiation

LC1-channel-4000x9000km-2km-0003 : cloud radiaton

LC1-channel-4000x9000km-2km-0004 : 2x cloud radiation

LC1-channel-4000x9000km-2km-0005 : restart at day 3

LC1-channel-4000x9000km-2km-0006 : restart at day 4

LC1-channel-4000x9000km-2km-0007 : restart at day 5

LC1-channel-4000x9000km-2km-0008 : restart at day 6

LC1-channel-4000x9000km-80km-0001 : full radiation

LC1-channel-4000x9000km-80km-0002 : no radiation

LC1-channel-4000x9000km-80km-0003 : cloud radiaton
