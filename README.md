# Keshtgar et al. (2022). Cloud-radiative impact on the dynamics and predictability of an idealized extratropical cyclone

Code repository for the ICON simulation run scripts, scripts for deriving baroclinic life cycle initial conditions, postprocessing of model output files, and the analysis scripts.
The associated data for the analysis will be available via KITopen of Karlsruhe Institute of Technology.
